﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EmployeeController : ControllerBase
    {

        [HttpGet]
        [Route("getEmployeeData")]
        public JsonResult getEmployeeData()
        {
            var UserId = "";
            var currentUser = HttpContext.User;

            if (currentUser.HasClaim(c => c.Type == "UserId"))
            {
                 UserId = currentUser.Claims.FirstOrDefault(c => c.Type == "UserId").Value;
                return new JsonResult(new { UserId = UserId,UserName = "Norman",Address= "Banglore"});
            }

            return new JsonResult(UserId);
        }

    }
}