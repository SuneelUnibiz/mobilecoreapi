﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreApi.CustomModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class OrderController : ControllerBase
    {

        [HttpGet]
        [Route("getOrderInfo")]
        public JsonResult getOrderInfo()
        {
            List<OrderDetailcs> orderList = new List<OrderDetailcs>();

            OrderDetailcs order1 = new OrderDetailcs() { OrderId ="10155-GS",Description= "Atlas COPCO XAS 90",Manfacturer= "ATLAS",DateInService="8-20-2015",Customer="Dell",Model="XAS 90",purchaseDate="21-09-2020",Status="On Rental"};
            orderList.Add(order1);
            OrderDetailcs order2 = new OrderDetailcs() { OrderId = "10156-GS", Description = "Atlas COPCO XAS 91", Manfacturer = "ATLAS", DateInService = "7-11-2016", Customer = "Google", Model = "XAS 91", purchaseDate = "01-04-2019", Status = "Available" };
            orderList.Add(order2);
            OrderDetailcs order3 = new OrderDetailcs() { OrderId = "10157-GS", Description = "Atlas COPCO XAS 92", Manfacturer = "ATLAS", DateInService = "8-09-2019", Customer = "Micro Soft", Model = "XAS 92", purchaseDate = "13-07-2020", Status = "On Rental" };
            orderList.Add(order3);
            OrderDetailcs order4 = new OrderDetailcs() { OrderId = "10158-GS", Description = "Atlas COPCO XAS 93", Manfacturer = "ATLAS", DateInService = "11-09-2019", Customer = "Unibiz", Model = "XAS 93", purchaseDate = "11-08-2020", Status = "Available" };
            orderList.Add(order4);
            OrderDetailcs order5 = new OrderDetailcs() { OrderId = "10159-GS", Description = "Atlas COPCO XAS 95", Manfacturer = "ATLAS", DateInService = "13-10-2015", Customer = "Infosys", Model = "XAS 94", purchaseDate = "10-04-2020", Status = "On Rental" };
            orderList.Add(order5);
            return new JsonResult(orderList);
        }
    }
}