﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        [HttpGet]
        [Route("getStudentData")]
        public JsonResult getStudentData()
        {
                return new JsonResult(new { UserId = "1", UserName = "Norman", Address = "Banglore" });
            
        }
    }
}