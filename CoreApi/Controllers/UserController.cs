﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CoreApi.CustomModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace CoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IConfiguration _config;
        public UserController(IConfiguration config)
        {
            _config = config;
        }

        [HttpPost]
        [Route("Login")]
        public JsonResult Login([FromBody] UserInfo userInfo)
        {
            loginResponse logRes = new loginResponse();
            var IsAuth = AuthenticateUser(userInfo);
            if (IsAuth)
            {
                var tokenString = GenerateJSONWebToken(userInfo);
                logRes = new loginResponse() {Email = userInfo.email, IsAuth = IsAuth, Token = tokenString};
                return new JsonResult(logRes);
            }
            return new JsonResult(logRes);
        }

        [HttpPost]
        [Route("GetSumDate")]
        public JsonResult GetSumDate([FromBody] DateRange dateInfo)
        {
            DateRange response = new DateRange() { startDate= dateInfo.startDate,endDate = dateInfo.endDate,sumDate = dateInfo.startDate.ToString("dd-MM-yyyy")+" to "+dateInfo.endDate.ToString("dd-MM-yyyy")};
            return new JsonResult(response);
        }
        private string GenerateJSONWebToken(UserInfo userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[] {
                             new Claim(JwtRegisteredClaimNames.Sub, userInfo.email),
                             new Claim("UserId", userInfo.email),
                             new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                               };
            var token = new JwtSecurityToken(_config["Jwt:Issuer"], _config["Jwt:Issuer"], claims, expires: DateTime.Now.AddMinutes(120), signingCredentials: credentials);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        private bool AuthenticateUser(UserInfo login)
        {
            //Validate the User Credentials  
            //Demo Purpose, I have Passed HardCoded User Information  
            var result = false;
            if (login.email == "c@gmail.com" && login.password == "1234567")
            {
                result = true;
                return result;
            }
            return result;
        }
    }
}