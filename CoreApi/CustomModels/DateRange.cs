﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreApi.CustomModels
{
    public class DateRange
    {
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string sumDate { get; set; }
    }
}
