﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreApi.CustomModels
{
    public class OrderDetailcs
    {
        public string OrderId { get; set; }
        public string Description { get; set; }

        public string Status { get; set; }
        public string Manfacturer { get; set; }
        public string Model { get; set; }
         public string DateInService { get; set; }
        public string purchaseDate { get; set; }
        public string Customer { get; set; }
    }
}
